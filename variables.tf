variable "profile" {
    type = string
    default = "default"
}

variable "region" {
    description = "Region which is used in this project"
    type = string
    default = "us-east-1"
}

variable "vpc_cidr_block" {
    description = "VPC CIDR Block"
    type = string
    default = "10.0.0.0/16"
}

variable "instance_tenancy" {
    description = "Instance Tenancy"
    type = string
    default = "default"
}

variable "public_subnet_1_cidr_block" {
    description = "CIDR Block for Public Subnet 1"
    type = string
    default = "10.0.0.0/24"
}

variable "public_subnet_1_availability_zone" {
    description = "Availability zone for Public Subnet 1"
    type = string
    default = "us-east-1a"
}

variable "private_subnet_1_cidr_block" {
    description = "CIDR Block for Private Subnet 1"
    type = string
    default = "10.0.2.0/24"
}

variable "private_subnet_1_availability_zone" {
    description = "Availability zone for Private Subnet 1"
    type = string
    default = "us-east-1b"
}

variable "public_map_public_ip_on_launch" {
    type = bool
    default = true
}

variable "private_map_public_ip_on_launch" {
    type = bool
    default = false
}

variable "route_table_cidr_block" {
    description = "Route table CIDR Block"
    type = string
    default = "0.0.0.0/0"
}

variable "security_group_name" {
    description = "Security Group Name"
    type = string
    default = "petclinic"
}

variable "security_group_description" {
    description = "Security Group Description"
    type = string
    default = "Allow SSH and HTTP inbound traffic"
}

variable "image_id" {
    description = "Id of image "
    type = string
    default = "ami-0022f774911c1d690"
}

variable "instance_type" {
    description = "Type of instance"
    type = string
    default = "t2.micro"
}

variable "key_name" {
    description = "Key name"
    type = string
    default = "cursova"
}

variable "associate_public_ip_address" {
    type = bool
    default = true
}

variable "name_prefix" {
    description = "Prefix name"
    type = string
    default = "Petclinic"
}

variable "name_aws_security_group" {
    description = "AWS Security Group Name"
    type = string
    default = "elb_http"
}

variable "description_aws_security_group" {
    description = "AWS Security Group Description"
    type = string
    default = "Allow HTTP traffic to instances through Elastic Load Balancer"
}

variable "name_aws_elb" {
    description = "AWS Elb Name"
    type = string
    default = "petclinic-elb"
}

variable "min_size_autoscaling_group" {
    type = number
    default = 1
}

variable "desired_capacity_autoscaling_group" {
    type = number
    default = 2
}

variable "max_size_autoscaling_group" {
    type = number
    default = 4
}

variable "health_check_type" {
    type = string
    default = "ELB"
}

variable "name_autoscaling_policy" {
    type = string
    default = "petclinic_policy_up"
}

variable "scaling_adjustment_autoscaling_policy" {
    type = number
    default = 1
}

variable "adjustment_type_autoscaling_policy" {
    type = string
    default = "ChangeInCapacity"
}

variable "cooldown_autoscaling_policy" {
    type = number
    default = 60
}

variable "alarm_name_cloudwatch" {
    type = string
    default = "petclinic_cpu_alarm_up"
}

variable "comparison_operator_cloudwatch" {
    type = string
    default = "GreaterThanOrEqualToThreshold"
}

variable "evaluation_periods_cloudwatch" {
    type = string
    default = "2"
}

variable "metric_name_cloudwatch" {
    type = string
    default = "CPUUtilization"
}

variable "namespace_cloudwatch" {
    type = string
    default = "AWS/EC2"
}

variable "period_cloudwatch" {
    type = string
    default = "60"
}

variable "statistic_cloudwatch" {
    type = string
    default = "Average"
}

variable "threshold_cloudwatch" {
    type = string
    default = "50"
}

variable "alarm_description" {
    type = string
    default = "This metric monitor EC2 instance CPU utilization"
}

variable "name_policy_down" {
    type = string
    default = "petclinic_policy_down"
}

variable "scaling_adjustment_policy_down" {
    type = number
    default = -1
}

variable "adjustment_type_policy_down" {
    type = string
    default = "ChangeInCapacity"
}

variable "cooldown_policy_down" {
    type = number
    default = 60
}

variable "alarm_name_cloudwatch_down" {
    type = string
    default = "petclinic_cpu_alarm_down"
}

variable "comparison_operator_cloudwatch_down" {
    type = string
    default = "LessThanOrEqualToThreshold"
}

variable "evaluation_periods_cloudwatch_down" {
    type = string
    default = "2"
}

variable "metric_name_cloudwatch_down" {
    type = string
    default = "CPUUtilization"
}

variable "namespace_cloudwatch_down" {
    type = string
    default = "AWS/EC2"
}

variable "period_cloudwatch_down" {
    type = string
    default = "60"
}

variable "statistic_cloudwatch_down" {
    type = string
    default = "Average"
}

variable "threshold_cloudwatch_down" {
    type = string
    default = "5"
}

variable "out" {
    type = string
    default = "Everything is perfectly done."
}

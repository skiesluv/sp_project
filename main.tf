terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  profile = var.profile
  region  = var.region
}

resource "aws_vpc" "vpc" {
  cidr_block       = var.vpc_cidr_block
  instance_tenancy = var.instance_tenancy

  tags = {
    Name = "main"
  }
}

resource "aws_subnet" "public-subnet-1" {

  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.public_subnet_1_cidr_block
  map_public_ip_on_launch = var.public_map_public_ip_on_launch
  availability_zone       = var.public_subnet_1_availability_zone

  tags      = {
    Name    = "Public Subnet 1"
  }
}


resource "aws_subnet" "private-subnet-1" {

  vpc_id                   = aws_vpc.vpc.id
  cidr_block               = var.private_subnet_1_cidr_block
  map_public_ip_on_launch  = var.private_map_public_ip_on_launch
  availability_zone        = var.private_subnet_1_availability_zone

  tags      = {
    Name    = "Private Subnet 1"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "Gateway"
  }
}

resource "aws_default_route_table" "route_table" {
  default_route_table_id = aws_vpc.vpc.default_route_table_id
route {
    cidr_block = var.route_table_cidr_block
    gateway_id = aws_internet_gateway.gw.id
  }
tags = {
    Name = "Default route table"
  }
}

resource "aws_security_group" "petclinic" {
  name        = var.security_group_name
  description = var.security_group_description
  vpc_id      = aws_vpc.vpc.id
  ingress {
    description = "SSH traffic"
    protocol    = "tcp"
    self        = true
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {  
    description = "HTTP traffic"
    protocol    = "tcp"
    self        = true
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {  
    description = "Ping"
    protocol    = "icmp"
    self        = true
    from_port   = -1
    to_port     = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Security Group for Petclinic"
  }

}

resource "aws_launch_configuration" "petclinic" {
  name_prefix = var.name_prefix

  image_id = var.image_id
  instance_type = var.instance_type
  key_name = var.key_name

  security_groups = [ aws_security_group.petclinic.id ]
  associate_public_ip_address = var.associate_public_ip_address
   user_data = <<EOF
        #!/bin/bash
        yum update -y
        yum install -y docker
        service docker start
        docker pull teslaluv/spring-petclinic:latest
        docker run -d -p 80:8080 teslaluv/spring-petclinic:latest
        EOF
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group" "elb_http" {
  name        = var.name_aws_security_group
  description = var.description_aws_security_group
  vpc_id = aws_vpc.vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Allow HTTP through ELB Security Group"
  }
}

resource "aws_elb" "petclinic_elb" {
  name = var.name_aws_elb
  security_groups = [
    aws_security_group.elb_http.id
  ]
  subnets = [
    aws_subnet.public-subnet-1.id,
    aws_subnet.private-subnet-1.id
  ]

  cross_zone_load_balancing   = true

  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    interval = 30
    target = "HTTP:80/"
  }

  listener {
    lb_port = 80
    lb_protocol = "http"
    instance_port = "80"
    instance_protocol = "http"
  }

}

resource "aws_autoscaling_group" "petclinic" {
  name = "${aws_launch_configuration.petclinic.name}-asg"

  min_size             = var.min_size_autoscaling_group
  desired_capacity     = var.desired_capacity_autoscaling_group
  max_size             = var.max_size_autoscaling_group
  
  health_check_type    = var.health_check_type
  load_balancers = [
    aws_elb.petclinic_elb.id
  ]

  launch_configuration = aws_launch_configuration.petclinic.name

  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupTotalInstances"
  ]

  metrics_granularity = "1Minute"

  vpc_zone_identifier  = [
    aws_subnet.public-subnet-1.id,
    aws_subnet.private-subnet-1.id
  ]


  lifecycle {
    create_before_destroy = true
  }

  tag {
    key                 = "Name"
    value               = "petclinic"
    propagate_at_launch = true
  }

}

resource "aws_autoscaling_policy" "petclinic_policy_up" {
  name = var.name_autoscaling_policy
  scaling_adjustment = var.scaling_adjustment_autoscaling_policy
  adjustment_type = var.adjustment_type_autoscaling_policy
  cooldown = var.cooldown_autoscaling_policy
  autoscaling_group_name = aws_autoscaling_group.petclinic.name
}

resource "aws_cloudwatch_metric_alarm" "petclinic_cpu_alarm_up" {
  alarm_name = var.alarm_name_cloudwatch
  comparison_operator = var.comparison_operator_cloudwatch
  evaluation_periods = var.evaluation_periods_cloudwatch
  metric_name = var.metric_name_cloudwatch
  namespace = var.namespace_cloudwatch
  period = var.period_cloudwatch
  statistic = var.statistic_cloudwatch
  threshold = var.threshold_cloudwatch

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.petclinic.name
  }

  alarm_description = var.alarm_description
  alarm_actions = [ aws_autoscaling_policy.petclinic_policy_up.arn ]
}


resource "aws_autoscaling_policy" "petclinic_policy_down" {
  name = var.name_policy_down
  scaling_adjustment = var.scaling_adjustment_policy_down
  adjustment_type = var.adjustment_type_policy_down
  cooldown = var.cooldown_policy_down
  autoscaling_group_name = aws_autoscaling_group.petclinic.name
}

resource "aws_cloudwatch_metric_alarm" "petclinic_cpu_alarm_down" {
  alarm_name = var.alarm_name_cloudwatch_down
  comparison_operator = var.comparison_operator_cloudwatch_down
  evaluation_periods = var.evaluation_periods_cloudwatch_down
  metric_name = var.metric_name_cloudwatch_down
  namespace = var.namespace_cloudwatch_down
  period = var.period_cloudwatch_down
  statistic = var.statistic_cloudwatch_down
  threshold = var.threshold_cloudwatch_down

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.petclinic.name
  }

  alarm_description = "This metric monitor EC2 instance CPU utilization"
  alarm_actions = [ aws_autoscaling_policy.petclinic_policy_down.arn ]
}

output "elb_dns_name" {
  value = aws_elb.petclinic_elb.dns_name
}

output "out" {
  value = var.out
}
